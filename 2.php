<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 Básica</title>
        <script type="text/javascript">
        window.addEventListener("load",arranca);
        function arranca();
        
        </script>
    </head>
    <body
        <?php
        $longitud=rand(10,1000);
        print "<p>Longitud: $longitud </p>";
        echo '<br>';
        ?>
        <svg width="<?= $longitud?>px" height="10px">
        <?php 
        // Es una forma alternativa de imprimir
        echo '<line x1="1" y1="5" x2="'.$longitud.'" y2="5" stroke="black" stroke-width="10"></line>';
        ?>
        </svg>
    </body>
</html>
