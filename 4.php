<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 4 Básica</title>
        <script type="text/javascript">
        window.addEventListener("load",arranca);
        function arranca(){
            document.querySelector("line").addEventListener("click",salta);
        }
        function salta(event){
            var valor;
            valor=Math.random()*(100-1)+1;
            event.target.setAttribute("x2",valor);
        }
        </script>
    </head>
    <body
        <?php
        $longitud=rand(10,1000);
        print "<p>Longitud: $longitud </p>";
        echo '<br>';
        ?>
        <svg width="<?= $longitud?>px" height="10px">
        <?php 
        // Es una forma alternativa de imprimir
        echo '<line x1="1" y1="5" x2="'.$longitud.'" y2="5" stroke="black" stroke-width="10"></line>';
        ?>
        </svg>
    </body>
</html>
